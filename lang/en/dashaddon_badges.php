<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'dashaddon_badges', language 'en'
 *
 * @package   dashaddon_badges
 * @copyright 2019 bdecent gmbh <https://bdecent.de>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Dash report - Badges addon';
$string['datasource:badge_data_source'] = 'Badges';

$string['badge'] = 'Badge';
$string['badgeimage'] = 'Badge image';
$string['badgeimagelink'] = 'Badge link';
$string['origin'] = 'Origin';
$string['sitebadge'] = 'Site Badge';
$string['tablealias_bd'] = 'Badges';
$string['dateissued'] = 'Issued date';
$string['badgeurl'] = 'Badge URL';
$string['badgebutton'] = 'Badge button';
$string['viewbadge'] = 'View badge';
$string['badgeimageurl'] = 'Badge Image URL';